class TxnMetrics {
  var nanos = List.empty[Long]
  def put(d: Long) = nanos ::= d
  def totalMillis = nanos.sum/1000000.0
  def totalCount = nanos.length
  override def toString() = s"TxnMetrics(totalCount: $totalCount, totalMillis: $totalMillis)"
}

case class TrackResult[T](result: T, txnMetrics: TxnMetrics)

object DB {

  val metrics = new scala.util.DynamicVariable[TxnMetrics](null)

  def time[T](f: => T) = {
    val start = System.nanoTime()
    val r = f
    metrics.value.put(System.nanoTime() - start)
    r
  }

  def txn[T](f: => T): T = {
    if (metrics.value == null) {
      f
    } else {
      time(f)
    }
  }

  def track[T](f: => T) = {
    val newVal = Option(metrics.value).map(identity).getOrElse(new TxnMetrics)
    metrics.withValue(newVal) {
      val r = TrackResult(f, metrics.value)
      r
    }
  }

}

import DB._

object Test extends App {
  println("\nTesting timing wrapping several txns")
  val result = track {
    txn { println("  Hello,") }
    txn { println("  world!") }
    txn { println("  It's a beautiful day!") }
  }

  println(result.txnMetrics)

  println("\nTesting txn without wrapping in track")
  txn {
    println("  hi")
  }

  def myFunc() = {
    txn {
      println("  It's a beautiful day...")
    }
    txn {
      println("  even though it's raining.")
    }
  }

  println("\nTesting timing wrapping several txns, some called in functions")
  val result2 = track {
    txn { println("  Hello,") }
    txn { println("  world!") }
    myFunc()
  }

  println(result2.txnMetrics)

  println("\nTesting nested track blocks")
  val result3 = track {
    txn { println("  Hello,") }
    track {
      txn {
        println("  may I interject?")
      }
    }
    txn { println("  world!") }
    myFunc()
  }

  println(result3.txnMetrics)


}